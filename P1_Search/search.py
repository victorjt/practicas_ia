# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
	"""
	Search the deepest nodes in the search tree first.

	Your search algorithm needs to return a list of actions that reaches the
	goal. Make sure to implement a graph search algorithm.

	To get started, you might want to try some of these simple commands to
	understand the search problem that is being passed in:

	print("Start:", problem.getStartState())
	print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
	print("Start's successors:", problem.getSuccessors(problem.getStartState()))
	"""
	"*** YOUR CODE HERE ***"
	
	stack = util.Stack()  # Utiliza una pila como estructura de datos para la frontera
	startState = problem.getStartState()
	stack.push((startState, []))  # Inicializa la pila con el estado inicial y una lista vacía de acciones

	explored = set()  # Conjunto para almacenar los estados explorados

	while not stack.isEmpty():
		currentState, actions = stack.pop()  # Obtiene el estado actual y las acciones realizadas hasta el momento

		if problem.isGoalState(currentState):
			return actions  # Si el estado actual es el objetivo, devuelve las acciones

		if currentState not in explored:
			explored.add(currentState)  # Marca el estado como explorado

			successors = problem.getSuccessors(currentState)
			for nextState, action, _ in successors:
				if nextState not in explored:
					nextActions = actions + [action]  # Agrega la acción actual a la lista de acciones
					stack.push((nextState, nextActions))  # Agrega el próximo estado y las acciones a la pila.

	return []  # Si no se encuentra una solución, devuelve una lista vacía.


    #util.raiseNotDefined()

def breadthFirstSearch(problem):
	"""Search the shallowest nodes in the search tree first."""
	"*** YOUR CODE HERE ***"
	"""
	queue = util.Queue()  # Utiliza una cola como estructura de datos para la frontera
	startState = problem.getStartState()
	queue.push((startState, []))  # Inicializa la cola con el estado inicial y una lista vacía de acciones

	explored = set()  # Conjunto para almacenar los estados explorados

	while not queue.isEmpty():
		currentState, actions = queue.pop()  # Obtiene el estado actual y las acciones realizadas hasta el momento

		if problem.isGoalState(currentState):
			return actions  # Si el estado actual es el objetivo, devuelve las acciones

		if currentState not in explored:
			explored.add(currentState)  # Marca el estado como explorado

			successors = problem.getSuccessors(currentState)
			for nextState, action, _ in successors:
				if nextState not in explored:
					nextActions = actions + [action]  # Agrega la acción actual a la lista de acciones
					queue.push((nextState, nextActions))  # Agrega el próximo estado y las acciones a la cola

	return []  # Si no se encuentra una solución, devuelve una lista vacía
	"""
    
	frontier = util.Queue() # Se crea la frontera, como cola
	frontier.push((problem.getStartState(), [])) # Añade el estado inicial del problema a la cola "frontier" con una lista de acciones vacía
	explorado = [] # Registro de los estados explorados.
 
    # Mientras la frontera no esté vacía, se obtienen nodos y el camino actual
	while not frontier.isEmpty():
		node, path = frontier.pop()
		
		# Si se ha llegado al objetivo, se devuelvve el camino.
		if problem.isGoalState(node):
			return path
		
		# Si el nodo actual no ha sido explorado, se añade a la lista de explorados.
		if not node in explorado:
			explorado.append(node)
			
			# Se obtienen los sucesores del nodo actual y se agregar a la frontera con la accion.
			for coord, move, cost in problem.getSuccessors(node):
				frontier.push((coord, path + [move]))

	return []  # No se encontró solución
    
    #util.raiseNotDefined()

def uniformCostSearch(problem):
	"""Search the node of least total cost first."""
	"*** YOUR CODE HERE ***"
	priorityQueue = util.PriorityQueue()  # Utiliza una cola de prioridad como estructura de datos para la frontera
	startState = problem.getStartState()
	priorityQueue.push((startState, [], 0), 0)  # Inicializa la cola con el estado inicial, una lista vacía de acciones y costo acumulado

	explored = set()  # Conjunto para almacenar los estados explorados

	while not priorityQueue.isEmpty():
		currentState, actions, cost = priorityQueue.pop()  # Obtiene el estado actual, las acciones realizadas hasta el momento y el costo acumulado

		if problem.isGoalState(currentState):
			return actions  # Si el estado actual es el objetivo, devuelve las acciones

		if currentState not in explored:
			explored.add(currentState)  # Marca el estado como explorado

			successors = problem.getSuccessors(currentState)
			for nextState, action, stepCost in successors:
				if nextState not in explored:
					nextActions = actions + [action]  # Agrega la acción actual a la lista de acciones
					nextCost = cost + stepCost  # Calcula el nuevo costo acumulado
					priorityQueue.push((nextState, nextActions, nextCost), nextCost)  # Agrega el próximo estado, las acciones y el costo a la cola de prioridad

	return []  # Si no se encuentra una solución, devuelve una lista vacía

    #util.raiseNotDefined()

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
	"""Search the node that has the lowest combined cost and heuristic first."""
	"*** YOUR CODE HERE ***"
	
	"""
	priorityQueue = util.PriorityQueue()  # Utiliza una cola de prioridad como estructura de datos para la frontera
	startState = problem.getStartState()
	priorityQueue.push((startState, [], 0), 0)  # Inicializa la cola con el estado inicial, una lista vacía de acciones y costo acumulado

	explored = set()  # Conjunto para almacenar los estados explorados

	while not priorityQueue.isEmpty():
		currentState, actions, cost = priorityQueue.pop()  # Obtiene el estado actual, las acciones realizadas hasta el momento y el costo acumulado

		if problem.isGoalState(currentState):
			return actions  # Si el estado actual es el objetivo, devuelve las acciones

		if currentState not in explored:
			explored.add(currentState)  # Marca el estado como explorado

			successors = problem.getSuccessors(currentState)
			for nextState, action, stepCost in successors:
				if nextState not in explored:
					nextActions = actions + [action]  # Agrega la acción actual a la lista de acciones
					nextCost = cost + stepCost  # Calcula el nuevo costo acumulado
					heuristicValue = heuristic(nextState, problem)  # Calcula el valor heurístico
					totalPriority = nextCost + heuristicValue  # Prioridad total (costo acumulado + heurística)
					priorityQueue.push((nextState, nextActions, nextCost), totalPriority)  # Agrega el próximo estado, las acciones y el costo a la cola de prioridad

	return []  # Si no se encuentra una solución, devuelve una lista vacía

    
    
    #util.raiseNotDefined()
	
	"""
	
	frontier = util.PriorityQueue() # Creo una cola de prioridad.
	counts = util.Counter() # Contador para las evaluaciones heuristicas.
	current = (problem.getStartState(), []) # Almacenamos el estado inicial del problema.
	counts[str(current[0])] += heuristic(current[0], problem) # Calculamos y almacenamos la evaluacion heurisica del estado inicial.
	frontier.push(current, counts[str(current[0])]) # Agrego el estado inicial a la cola de prioridad.
	closed = [] # Lista para nodos explorados.

	while not frontier.isEmpty():
		node, path = frontier.pop() # Extraemos el nodo y su camino
		if problem.isGoalState(node):
			return path # Si es el objetivo, nos devuelve el camino.
		
		# Si no es el objetico, lo marcamos como explorado
		if not tuple(node) in closed:
			closed.append(tuple(node))
			
			# Generamos sucesores y exploramos cada uno de ellos.
			for coord, move, cost in problem.getSuccessors(node):
				newpath = path + [move]
				
				# Actualizamos la evaluacion heuristica.
				counts[str(coord)] = problem.getCostOfActions(newpath)
				counts[str(coord)] += heuristic(coord, problem)
				
				# Agregamos el sucesor y su camino a la frontera.
				frontier.push((coord, newpath), counts[str(coord)])
# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
